Feature: API Cardados

    Scenario: CT001_Validate Schema API SearchID
        Then I send Post with car_plate GDP1002
        And Valid response body

    Scenario: CT002_Validate Schema API SearchID
        Then I send Get with car_plate 2955013
        And Valid response body

    Scenario: CT003_Run Post in API Search with national car Plate and validate that Search ID Its present
        Given I configure in body request car_plate GDP1002
        When I send request Post
        Then I valid status response 200

    Scenario: CT004_Consult Report informing search ID valid and validate that data of car
        Given I configure  Params with pesquisa_id 2955013
        When I send request Get
        Then I valid that API returned car datas the plate researched
        And With resultado true
        And Request returned 200
        And With searchID sended in Params
        And With field placa that corresponds the searchID
        And With chassi string
        And With renavam string
        And With municipio string
        And With uf string
        And With cor string
        And With numero_motor string
        And With procedencia string
        And With graveme equal 0
        And With recall equal 0
        And With renaif string
        And In financeiro field multa like string
        And In financeiro field ipva like string
        And In financeiro field dpvat like string
        And In financeiro field licenciamento like string
        And In financeiro field Mensagem like string


    Scenario: CT005_Try consult search id informing a license plate that doesnt exist
        Given I configure in body with invalid car_plate FGT@00@
        When I send request Post
        Then I valid that API return code 422
        And Message car_plate not valid

    Scenario: CT006_Try consult search id without informing a license plate
        Given I configure in body without send car_plate
        When I send request Post
        Then I valid that API return code 400
        And Message car_plate can not be blank

    Scenario: CT007_Try Run Post in API Search with national car Plate When we have problem with reply the server
        Given I configure in body request car_plate 1003
        When I send Post
        Then I valid return code 422
        And Negative message is car_plate not valid

    Scenario: CT008_Try Consult Report When we have problem with reply the server
        And I configure  Params with pesquisa_id 296
        And I send request Get
        Then I valid that API return code 500
        And Message upstream error

