import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import { access } from "fs";
import { url } from "inspector";
import { type } from "os";
import { parseJsonText } from "typescript";

const URL_REPORT = Cypress.env("REPORT");

Given("I configure  Params with pesquisa_id 2955013", () => {});
When("I send request Get", () => {});
Then("I valid that API returned car datas the plate researched", () => {
  cy.request({
    method: "GET",
    qs: {
      pesquisa_id: 2955013,
    },
    url: URL_REPORT,
  }).as("response");
  And("With resultado true", () => {
    cy.get("@response").then((res) => {
      expect(res.body).to.have.property("resultado", true);
    });
  });
  And("Request returned 200", () => {
    cy.get("@response").then((res) => {
      expect(res.status).to.be.equal(200);
    });
  });
  And("With searchID sended in Params", () => {
    cy.get("@response").then((res) => {
      expect(res.body).to.have.property("pesquisa_id", "2955013");
    });
    And("With field placa that corresponds the searchID", () => {
      cy.get("@response").then((res) => {
        expect(res.body).to.have.property("placa", "QJV8777");
      });
      And("With chassi string", () => {
        cy.get("@response").then((res) => {
          expect(res.body.chassi).to.be.a("string").not.be.empty;
        });
        And("With renavam string", () => {
          cy.get("@response").then((res) => {
            expect(res.body.renavam).to.be.a("string").not.be.empty;
          });
          And("With municipio string", () => {
            cy.get("@response").then((res) => {
              expect(res.body.municipio).to.be.a("string").not.be.empty;
            });
            And("With uf string", () => {
              cy.get("@response").then((res) => {
                expect(res.body.uf).to.be.a("string").not.be.empty;
              });
              And("With cor string", () => {
                cy.get("@response").then((res) => {
                  expect(res.body.cor).to.be.a("string").not.be.empty;
                });
                And("With numero_motor string", () => {
                  cy.get("@response").then((res) => {
                    expect(res.body.numero_motor).to.be.a("string").not.be
                      .empty;
                  });
                  And("With procedencia string", () => {
                    cy.get("@response").then((res) => {
                      expect(res.body.procedencia).to.be.a("string").not.be
                        .empty;
                    });
                    And("With graveme equal 0", () => {
                      cy.get("@response").then((res) => {
                        expect(res.body).to.have.property("graveme", 0);
                      });
                      And("With recall equal 0", () => {
                        cy.get("@response").then((res) => {
                          expect(res.body).to.have.property("recall", 0);
                        });
                        And("With renaif string", () => {
                          cy.get("@response").then((res) => {
                            expect(res.body.renaif).to.be.a("string").not.be
                              .empty;
                          });
                          And("In financeiro field multa like string", () => {
                            cy.get("@response").then((res) => {
                              expect(res.body.financeiro.multa).to.be.a(
                                "string"
                              ).not.be.empty;
                            });
                            And("In financeiro field ipva like string", () => {
                              cy.get("@response").then((res) => {
                                expect(res.body.financeiro.ipva).to.be.a(
                                  "string"
                                ).not.be.empty;
                              });
                              And(
                                "In financeiro field dpvat like string",
                                () => {
                                  cy.get("@response").then((res) => {
                                    expect(res.body.financeiro.dpvat).to.be.a(
                                      "string"
                                    ).not.be.empty;
                                  });
                                  And(
                                    "In financeiro field licenciamento like string",
                                    () => {
                                      cy.get("@response").then((res) => {
                                        expect(
                                          res.body.financeiro.licenciamento
                                        ).to.be.a("string");
                                      });
                                      And(
                                        "In financeiro field Mensagem like string",
                                        () => {
                                          cy.get("@response").then((res) => {
                                            expect(
                                              res.body.financeiro.Mensagem
                                            ).to.be.a("string").to.be.empty;
                                          });
                                        }
                                      );
                                    }
                                  );
                                }
                              );
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});

Given("I configure  Params with pesquisa_id 296", () => {});
When("I send request Get", () => {});
Then("I valid that API return code 500", () => {
  cy.request({
    method: "GET",
    failOnStatusCode: false,
    qs: {
      pesquisa_id: 296,
    },
    url: URL_REPORT,
  }).as("response");
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(500);
  });
});
    And("Message upstream error", () => {
      cy.get("@response").then((res) => {
        expect(res.body.message).to.be.equal("upstream error");
     
  });
});
