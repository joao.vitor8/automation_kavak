import { and } from "ajv/dist/compile/codegen";
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const SEARCH = Cypress.env("SEARCH");
const REPORT = Cypress.env("REPORT");

const Ajv = require("ajv");
const ajv = new Ajv({ allErrors: true, verbose: true }); // options can be passed, e.g. {allErrors: true}

Then("I send Post with car_plate GDP1002", () => {
  cy.request({
    method: "POST",
    url: SEARCH,
    body: {
      car_plate: "GDP1002",
    },
  }).then((response) => {
    cy.fixture("searchid").then((searchid) => {
      const validate = ajv.compile(searchid);
      const valid = validate(response.body);
      if (!valid)
        cy.log(validate.errors).then(() => {
          throw new Error("Contract Failed");
        });
    });
  });
});
And("Valid response body", () => {});

Then("I send Get with car_plate 2955013", () => {
  cy.request({
    method: "GET",
    qs: {
      pesquisa_id: 2955013,
    },
    url: REPORT,
  })
  .then((response) => {
    cy.fixture("report").then((report) => {
      const validate = ajv.compile(report);
      const valid = validate(response.body);
      if (!valid)
        cy.log(validate.errors).then(() => {
          throw new Error("Contract Failed");
        });
    });
  });
});
And("Valid response body", () => {});
