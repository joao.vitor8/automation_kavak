import { expect } from "chai";
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const URL_SEARCH = Cypress.env("SEARCH");

Given("I configure in body request car_plate GDP1002", () => {});
When("I send request Post", () => {});
Then("I valid status response 200", () => {
  cy.request({
    method: "POST",
    url: URL_SEARCH,
    body: {
      car_plate: "GDP1002",
    },
  })
    .as("response")
    .its("body.pesquisa_id")
    .should("not.be.empty");

  cy.get("@response").then((res) => {
    expect(res.body).to.have.property("placa", "GDP1002");
  });
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(200);
  });
});

Given("I configure in body with invalid car_plate FGT@00@", () => {});
When("I send request Post", () => {});
Then("I valid that API return code 422", () => {
  cy.request({
    method: "POST",
    failOnStatusCode: false,
    url: URL_SEARCH,
    body: {
      car_plate: "FGT@00@",
    },
  }).as("response");
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(422);
  });
});
And("Message car_plate not valid", () => {
  cy.get("@response").then((res) => {
    expect(res.body.message).to.be.equal("car_plate not valid");
  });
});

Given("I configure in body without send car_plate", () => {});
When("I send request Post", () => {});
Then("I valid that API return code 400", () => {
  cy.request({
    method: "POST",
    failOnStatusCode: false,
    url: URL_SEARCH,
    body: {
      car_plate: "",
    },
  }).as("response");
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(400);
  });
});
And("Message car_plate can not be blank", () => {
  cy.get("@response").then((res) => {
    expect(res.body.message).to.be.equal("car_plate can not be blank");
  });
});

Given("I configure in body request car_plate 1003", () => {});
When("I send request Post", () => {});
Then("I valid that API return code 524", () => {
  cy.request({
    method: "POST",
    failOnStatusCode: false,
    url: URL_SEARCH,
    body: {
      car_plate: "1003",
    },
  }).as("response");
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(524);
  });
});

Given("I configure in body request car_plate 1003", () => {});
When("I send Post", () => {});
Then("I valid return code 422", () => {
  cy.request({
    method: "POST",
    failOnStatusCode: false,
    url: URL_SEARCH,
    body: {
      car_plate: "1003",
    },
  }).as("response");
  cy.get("@response").then((res) => {
    expect(res.status).to.be.equal(422);
  });
});
Then("Negative message is car_plate not valid", () => {
  cy.get("@response").then((res) => {
    expect(res.body.message).to.be.equal("car_plate not valid");
  });
});

