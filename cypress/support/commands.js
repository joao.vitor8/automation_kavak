// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
const URL_TOKEN = Cypress.env("API_BASE_TOKEN");
const URL_SEARCH = Cypress.env("API_BASE_SEARCH");
const URL_SEARCH_PRI = Cypress.env("API_BASE_SEARCH_PRI");

Cypress.Commands.add("getToken", () => {
  cy.request({
    method: "POST",
    url: URL_TOKEN,
    headers: {
      Accept: "application/json",
      Authorization: "Basic Q2FyRGFkb3NBUEk6WTR0ZlhNWjZiQmQ3QDREPQ==",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: {
      client_id: "CarDadosAPI",
      grant_type: "client_credentials",
    },
  })
    .its("body.access_token")
    .should("not.be.empty")
    .then((access_token) => {
      return access_token;
    });
});
Cypress.Commands.add("getSearchId", () => {
  cy.getToken().then((access_token) => {
    cy.request({
      method: "POST",
      url: URL_SEARCH,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
      body: {
        car_plate: "FGP4465",
      },
    })
      .its("body.pesquisa_id")
      .should("not.be.empty")
      .then((pesquisa_id) => {
        return pesquisa_id;
      });
  });
});
Cypress.Commands.add("getPlate", () => {
  cy.getToken().then((access_token) => {
    cy.request({
      method: "POST",
      url: URL_SEARCH,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
      body: {
        car_plate: "FGP4465",
      },
    })
      .its("body.placa")
      .should("not.be.empty")
      .then((placa) => {
        return placa;
      });
  });
});
Cypress.Commands.add("getSearchId_PRI", () => {
  cy.getToken().then((access_token) => {
    cy.request({
      method: "POST",
      url: URL_SEARCH_PRI,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
      body: {
        car_plate: "FGP4465",
      },
    })
      .its("body.pesquisa_id")
      .should("not.be.empty")
      .then((pesquisa_id) => {
        return pesquisa_id;
      });
  });
});
Cypress.Commands.add("getPlate_PRI", () => {
  cy.getToken().then((access_token) => {
    cy.request({
      method: "POST",
      url: URL_SEARCH_PRI,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
      body: {
        car_plate: "FGP4465",
      },
    })
      .its("body.placa")
      .should("not.be.empty")
      .then((placa) => {
        return placa;
      });
  });
});

